package com.example.SecondLab;

import Database.ModelCar;
import Database.MyDBConnection;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "getCarsServlet", value = "/get-cars-servlet")
public class GetCarsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public GetCarsServlet(){
        super();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<table>");

        try{
            MyDBConnection.Connection();
            MyDBConnection.CreateCarsTable();
            //MyDBConnection.WriteCarsTable();
            ArrayList<ModelCar> cars = MyDBConnection.ReadCarsTable();
            HtmlHelper.WriteCarsDataHtml(out, cars, "Усі машини");



            ArrayList<ModelCar> carsWithConcreteNumber = MyDBConnection.ReadCarsTableWithConcreteNumber();
            HtmlHelper.WriteCarsDataHtml(out, carsWithConcreteNumber, "Відфільтрованні машини");

            MyDBConnection.CloseDB();
        }
        catch (Exception ex){
            out.println(ex.getMessage());
        }

        out.println("<tr>");
        out.println("<td colspan=\"12\" align=\"right\">");
        out.println("<form name=\"frm\" " +
                "method=\"get\" action=\"http://localhost:8081/SecondLab/StartPage.html\">\n" +
                "                    <input type=\"submit\" value=\"Назад\">\n" +
                "                </form>");
        out.println("</td>");
        out.println("</table>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}