package com.example.SecondLab;

import Database.ModelCar;

import java.io.PrintWriter;
import java.util.ArrayList;

public class  HtmlHelper {

    public static void WriteCarsDataHtml(PrintWriter out, ArrayList<ModelCar> cars, String typeOfCars){
        out.println("<tr>");
        out.println("<td colspan=\"12\">");
        out.println(typeOfCars);
        out.println("<td>");
        out.println("</tr>");

        for (ModelCar car : cars) {
            out.println("<tr>");
            WriteTableDataHtml(out, "Номер", car.Number);
            WriteTableDataHtml(out, "Марка", car.Label);
            WriteTableDataHtml(out, "Модель", car.Model);
            WriteTableDataHtml(out, "Дата випуску", car.YearCreated);
            WriteTableDataHtml(out, "Адреса власника", car.AddressOwner);
            WriteTableDataHtml(out, "Прізвище власника", car.SurnameOwner);
            out.println("</tr>");
        }
    }

    private static void WriteTableDataHtml(PrintWriter out, String label, String data){
        out.println("<td>");
        out.println(label+":");
        out.println("</td>");
        out.println("<td>");
        out.println(data);
        out.println("</td>");
    }
}
