package Database;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MyDBConnection {
    public static Connection conn;
    public static Statement statmt;
    public static ResultSet resSet;

    public static String Connection() throws ClassNotFoundException, SQLException
    {
        conn = null;
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:CarsDB.sqlite");

        return "Connection with db";
    }

    public static String CreateCarsTable() throws ClassNotFoundException, SQLException
    {
        statmt = conn.createStatement();
        statmt.execute("CREATE TABLE " +
                "if not exists 'myCars' " +
                "('id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "'yearCreated' text, " +
                "'label' text," +
                "'model' text, 'number' text, 'addressOwner' text, 'surnameOwner' text);");

        return "Table have been created or already exist";
    }

    public static String WriteCarsTable() throws SQLException
    {
        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                                        "'number', 'addressOwner', 'surnameOwner') " +
                                        "VALUES ('1996', 'Fiat', 'Clio'," +
                        "                '1234', 'Chernivtsi', 'Random A.A.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('1998', 'Reno', 'Logan'," +
                        "                '5555', 'Chernivtsi Holovna str', 'Random A.B.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('2010', 'Mercedes', 'Sprinter'," +
                        "                '6666', 'Chernivtsi Holovna str', 'Random A.C.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('2003', 'Suzuki', 'Jimmny'," +
                        "                '5656', 'Chernivtsi Holovna str', 'Random A.D.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('2010', 'Ford', 'Focus'," +
                        "                '1234', 'Chernivtsi Holovna str', 'Random A.E.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('2007', 'Mazda', 'RX8'," +
                        "                '1563', 'Chernivtsi Holovna str', 'Random A.F.'); ");

        statmt.execute(
                "INSERT INTO 'myCars' ('yearCreated', 'label', 'model'," +
                        "'number', 'addressOwner', 'surnameOwner') " +
                        "VALUES ('2012', 'Mazda', 'RX8'," +
                        "                '5566', 'Chernivtsi Holovna str', 'Random A.G.'); ");

        return "Write db data";
    }

    public static ArrayList<ModelCar> ReadCarsTable() throws ClassNotFoundException, SQLException
    {
        ArrayList<ModelCar> cars = new ArrayList<>();
        resSet = statmt.executeQuery("SELECT * FROM myCars");

        while(resSet.next())
        {
            ModelCar car = new ModelCar();
            car.YearCreated = resSet.getString("yearCreated");
            car.Label = resSet.getString("label");
            car.Model = resSet.getString("model");
            car.Number = resSet.getString("number");
            car.AddressOwner = resSet.getString("addressOwner");
            car.SurnameOwner = resSet.getString("surnameOwner");
            cars.add(car);
        }

        return  cars;
    }

    public static ArrayList<ModelCar> ReadCarsTableWithConcreteNumber() throws ClassNotFoundException, SQLException
    {
        ArrayList<ModelCar> carsWithConcreteNumber = new ArrayList<>();
        resSet = statmt.executeQuery("SELECT * FROM myCars WHERE \n" +
                "number GLOB '*[5-6]*' \n" +
                "AND number NOT GLOB '*[0-4]*' \n" +
                "AND number NOT GLOB '*[7-9]*'\n" +
                "AND number LIKE '_%'" +
                "ORDER BY yearCreated ASC;");

        while(resSet.next())
        {
            ModelCar car = new ModelCar();
            car.YearCreated = resSet.getString("yearCreated");
            car.Label = resSet.getString("label");
            car.Model = resSet.getString("model");
            car.Number = resSet.getString("number");
            car.AddressOwner = resSet.getString("addressOwner");
            car.SurnameOwner = resSet.getString("surnameOwner");
            carsWithConcreteNumber.add(car);
        }

        return  carsWithConcreteNumber;
    }

    public static String CloseDB() throws ClassNotFoundException, SQLException
    {
        conn.close();
        statmt.close();
        resSet.close();

        return "Connection have been closed";
    }
}
