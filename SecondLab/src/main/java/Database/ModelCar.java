package Database;

import java.util.Date;

public class ModelCar {

    public String Number;

    public String Label;

    public String Model;

    public String YearCreated;

    public String SurnameOwner;

    public String AddressOwner;

    @Override
    public String toString() {
        return "ModelCar{" +
                "Number='" + Number + '\'' +
                ", Label='" + Label + '\'' +
                ", Model='" + Model + '\'' +
                ", YearCreated=" + YearCreated +
                ", SurnameOwner='" + SurnameOwner + '\'' +
                ", AddressOwner='" + AddressOwner + '\'' +
                '}';
    }
}
