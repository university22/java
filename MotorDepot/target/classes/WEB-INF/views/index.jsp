<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
<div align="center">
    <h2>Car list</h2>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Model</th>
            <th>Carrying</th>
            <th>Serviceable</th>
        </tr>
        <div><p>Test 1</p></div>
        <c:catch var ="exception">
            <c:forEach items="${listCar}" var="car">
                <tr>
                    <td>${car.id}</td>
                    <td>${car.model}</td>
                    <td>${car.carrying}</td>
                    <td>${car.serviceable}</td>
                </tr>
                <div><p>Test 1</p></div>
            </c:forEach>
        </c:catch>
        <c:if test = "${exception != null}">
            <p>Возникло исключение : ${exception} <br />
                Сообщение : ${exception.message}</p>
        </c:if>
    </table>
</div>
</body>
</html>