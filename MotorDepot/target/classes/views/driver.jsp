<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Driver page</title>
</head>
<body>
    <div align="center">
    <br>
        <br>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Driver info</h5>
                <p class="card-text"><b>Id:</b> ${driver.id}</p>
                <p class="card-text"><b>Surname:</b> ${driver.surname}</p>
                <p class="card-text"><b>Car id:</b> ${driver.carId}</p>
                <a href="car/${driver.carId}" class="btn btn-primary">Manage my car</a>
            </div>
        </div>
        <br>
        <br>
        <p>List of my request</p>
        <table border="1" cellpadding="5">
            <tr>
                <th>ID</th>
                <th>Cargoweight</th>
                <th>Completed</th>
                <th>Report</th>
            </tr>
            <div></div>
                <c:forEach items="${listRequest}" var="request">
                    <tr>
                        <td>${request.id}</td>
                        <td>${request.cargoweight}</td>
                        <td>${request.completed}</td>
                        <c:choose>
                            <c:when test="${request.completed == 'false'}">
                                <td><a href="done/${request.id}" class="btn btn-primary">Set done</a></td>
                            </c:when>
                            <c:otherwise>
                                <td><a class="btn btn-secondary disabled" aria-disabled="true">Donned</a></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
        </table>
    </div>
</body>
</html>
