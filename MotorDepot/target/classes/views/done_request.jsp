<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Edit Request</title>
</head>
<body>
<div align="center">
    <h2>Edit Request</h2>
    <form:form action="save" method="post" modelAttribute="request">
        <table border="0" cellpadding="5">
            <tr>
                <td>ID: </td>
                <td>${request.id}
                    <form:hidden path="id"/>
                </td>
            </tr>
            <tr>
                <td>Cargoweight: </td>
                <td>${request.cargoweight}
                    <form:hidden path="cargoweight"/>
                </td>
            </tr>
            <tr>
                <td>Driver: </td>
                <td>${request.driverId}
                    <form:hidden path="driverId"/>
                </td>
            </tr>
            <tr>
                <td>Completed: </td>
                <td>
                    <td><form:checkbox path="completed" /></td>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit"></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
