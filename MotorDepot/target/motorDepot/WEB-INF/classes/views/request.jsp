<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Requests</title>
</head>
<body>
<div align="center">
    <br>
    <p>List of requests</p>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Cargoweight</th>
            <th>Completed</th>
            <th>Driver id</th>
            <th>Assign driver</th>
        </tr>
        <div></div>
        <c:forEach items="${listRequest}" var="request">
            <tr>
                <td>${request.id}</td>
                <td>${request.cargoweight}</td>
                <td>${request.completed}</td>
                <td>${request.driverId}</td>
                <c:choose>
                    <c:when test="${request.driverId == '0'}">
                        <td><a href="request/${request.id}" class="btn btn-primary">Assign driver</a></td>
                    </c:when>
                    <c:otherwise>
                        <td><a class="btn btn-secondary disabled" aria-disabled="true">Assign driver</a></td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>