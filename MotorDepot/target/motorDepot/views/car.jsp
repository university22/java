<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Car page</title>
</head>
<body>
<div align="center">
    <p></p>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Car state</h5>
            <p class="card-text"><b>Id:</b> ${car.id}</p>
            <p class="card-text"><b>Model:</b> ${car.model}</p>
            <p class="card-text"><b>Carrying:</b> ${car.carrying}</p>
            <p class="card-text"><b>Serviceable:</b> ${car.serviceable}</p>
            <a href="edit/${car.id}" class="btn btn-primary">Change car state</a>
        </div>
    </div>
</div>
</body>
</html>
