package chnu.third.code.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer driverId;
    private Integer cargoweight;
    private Boolean completed;

    public Request(){

    }

    protected Request(Integer driverId, Integer cargoweight, Boolean completed){
        this.cargoweight = cargoweight;
        this.driverId = driverId;
        this.completed = completed;
    }

    public Integer getCargoweight() {
        return cargoweight;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public  void setDriverId(Integer driverId){
        this.driverId = driverId;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public void setCargoweight(Integer cargoweight) {
        this.cargoweight = cargoweight;
    }
}
