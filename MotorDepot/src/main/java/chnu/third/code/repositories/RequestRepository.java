package chnu.third.code.repositories;

import chnu.third.code.models.Request;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends CrudRepository<Request, Integer> {
    @Query("SELECT u FROM Request u WHERE u.driverId = ?1")
    public List<Request> getRequestByDriverId(@Param("driverId") Integer driverId);
}
