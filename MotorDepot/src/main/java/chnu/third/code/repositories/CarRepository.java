package chnu.third.code.repositories;

import chnu.third.code.models.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  CarRepository extends CrudRepository<Car, Integer>{
}
