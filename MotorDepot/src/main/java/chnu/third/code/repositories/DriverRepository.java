package chnu.third.code.repositories;

import chnu.third.code.models.Driver;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  DriverRepository extends CrudRepository<Driver, Integer>{
    @Query("SELECT driver FROM Driver driver, Car car " +
            "WHERE car.carrying >= ?1 AND car.serviceable = true " +
            "AND driver.carId = car.id")
    public List<Driver> getDriverWithSiutableCar(@Param("cargoWeight") Integer carWeight);
}
