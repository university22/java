package chnu.third.code.controllers;

import chnu.third.code.models.Driver;
import chnu.third.code.models.Request;
import chnu.third.code.services.DriverService;
import chnu.third.code.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/request")
public class RequestController {
    private final RequestService requestService;
    private  final DriverService driverService;

    @Autowired
    public RequestController(RequestService requestService, DriverService driverService) {
        this.requestService = requestService;
        this.driverService = driverService;
    }

    @RequestMapping
    public ModelAndView getAll(){
        List<Request> listRequest = requestService.listAll();
        ModelAndView mav = new ModelAndView("request");
        mav.addObject("listRequest", listRequest);
        return mav;
    }

    @RequestMapping("{id}")
    public ModelAndView getRequest(@PathVariable Integer id) {
        Request request = requestService.get(id);
        //<Driver> listDriver = driverService.listAll();
        List<Driver> listDriver = driverService.getAllWithSuitableCar(request.getCargoweight());
        ModelAndView mav = new ModelAndView("edit_request");
        mav.addObject("listDriver",listDriver);
        mav.addObject("request", request);
        return mav;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(@ModelAttribute("request") Request request) {
        requestService.save(request);
        return "redirect:/request";
    }
}
