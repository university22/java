package chnu.third.code.controllers;

import chnu.third.code.models.Car;
import chnu.third.code.models.Driver;
import chnu.third.code.models.Request;
import chnu.third.code.services.CarService;
import chnu.third.code.services.DriverService;
import chnu.third.code.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/driver")
public class DriverController {
    private final CarService carService;
    private final RequestService requestService;
    private  final DriverService driverService;

    @Autowired
    public DriverController(CarService carService, RequestService requestService, DriverService driverService) {
        this.carService = carService;
        this.requestService = requestService;
        this.driverService = driverService;
    }

    @ModelAttribute("listCar")
    public List<Car> getCars(){
        return new ArrayList<>();
    }

    @RequestMapping("{id}")
    public ModelAndView home(@PathVariable Integer id) {

        List<Car> listCar = carService.listAll();
        Driver driver = driverService.get(id);
        List<Request> listRequest = requestService.getAllByDriverId(id);
        ModelAndView mav = new ModelAndView("driver");
        mav.addObject("listCar", listCar);
        mav.addObject("driver", driver);
        mav.addObject("listRequest", listRequest);
        return mav;
    }

    @RequestMapping(value = "car/{id}")
    public String getDriverCar(@PathVariable Integer id) {
        return "redirect:/car/"+id;
    }

    @RequestMapping("done/{id}")
    public ModelAndView doneRequest(@PathVariable Integer id) {
        Request request = requestService.get(id);
        ModelAndView mav = new ModelAndView("done_request");
        mav.addObject("request", request);
        return mav;
    }

    @RequestMapping(value = "done/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("request") Request request) {
        requestService.save(request);
        return "redirect:/driver/"+request.getDriverId();
    }
}
