package chnu.third.code.controllers;

import chnu.third.code.models.Car;
import chnu.third.code.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping("{id}")
    public ModelAndView getCar(@PathVariable Integer id) {
        Car car = carService.get(id);
        ModelAndView mav = new ModelAndView("car");
        mav.addObject("car", car);
        return mav;
    }

    @RequestMapping("edit/{id}")
    public ModelAndView editCar(@PathVariable Integer id) {
        Car car = carService.get(id);
        ModelAndView mav = new ModelAndView("edit_car");
        mav.addObject("car", car);
        return mav;
    }

    @RequestMapping(value = "edit/save", method = RequestMethod.POST)
    public String saveCar(@ModelAttribute("car") Car car) {
        carService.save(car);
        /*ModelAndView mav = new ModelAndView("car");
        mav.addObject("car", car);*/
        return "redirect:/car/"+car.getId();
    }
}


