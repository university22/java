package chnu.third.code.controllers;

import chnu.third.code.models.Car;
import chnu.third.code.models.Driver;
import chnu.third.code.services.CarService;
import chnu.third.code.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class StartController {
    private final CarService carService;

    private final DriverService driverService;

    @Autowired
    public StartController(CarService carService, DriverService driverService) {
        this.carService = carService;
        this.driverService = driverService;
    }

    @RequestMapping
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "auth")
    public String auth(@RequestParam Integer driverId) {
        return "redirect:/driver/"+driverId;
    }
}