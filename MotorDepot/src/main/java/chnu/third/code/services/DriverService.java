package chnu.third.code.services;

import chnu.third.code.models.Driver;
import chnu.third.code.repositories.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DriverService {
    final
    DriverRepository repo;

    @Autowired
    public DriverService(DriverRepository repo) {
        this.repo = repo;
    }

    public void save(Driver driver) {
        repo.save(driver);
    }

    public List<Driver> listAll() {
        return (List<Driver>) repo.findAll();
    }

    public Driver get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

    public  List<Driver> getAllWithSuitableCar(Integer cargoWeight) { return  repo.getDriverWithSiutableCar(cargoWeight);}
}
