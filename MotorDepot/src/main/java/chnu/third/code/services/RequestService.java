package chnu.third.code.services;

import chnu.third.code.models.Driver;
import chnu.third.code.models.Request;
import chnu.third.code.repositories.DriverRepository;
import chnu.third.code.repositories.RequestRepository;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RequestService {
    final
    RequestRepository repo;

    @Autowired
    public RequestService(RequestRepository repo) {
        this.repo = repo;
    }

    public void save(Request request) {
        repo.save(request);
    }

    public List<Request> listAll() {
        return (List<Request>) repo.findAll();
    }

    public Request get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

    public  List<Request> getAllByDriverId(Integer driverId){return  repo.getRequestByDriverId(driverId);}
}
