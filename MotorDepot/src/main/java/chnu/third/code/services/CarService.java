package chnu.third.code.services;

import chnu.third.code.models.Car;
import chnu.third.code.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarService {
    final
    CarRepository repo;

    @Autowired
    public CarService(CarRepository repo) {
        this.repo = repo;
    }

    public void save(Car car) {
        repo.save(car);
    }

    public List<Car> listAll() {
        return (List<Car>) repo.findAll();
    }

    public Car get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}