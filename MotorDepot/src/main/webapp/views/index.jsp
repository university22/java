<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Start page</title>
</head>
<body>
<div align="center">
    <h2>Home page</h2>
    <a href="request" class="btn btn-primary"> I`m manager</a>
    <br>
    <br>
    <hr>
    <form method="get" action="auth">
        <input type="text" name="driverId" />
        <input class="btn btn-primary" type="submit" value="Auth as driver" />
    </form>
</div>
</body>
</html>