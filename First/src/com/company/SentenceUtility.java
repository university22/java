package com.company;

import java.util.Arrays;
import java.util.Comparator;

public class SentenceUtility {
    public static String SortWords(String sentence) {
        String[] words = sentence.split(" ");
        Arrays.sort(words, Comparator.comparing(String::length));
        return String.join(" ", words);
    }
}