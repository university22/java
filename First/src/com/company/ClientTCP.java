package com.company;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClientTCP {
    public static void main(String[] args) {
        try {
            Socket clientSocket = new Socket("localhost", 1500);
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("Client started");

            while (true) {
                if (br.ready()) {
                    String clientData = br.readLine();
                    if (clientData.split(" ").length < 5 && !clientData.equalsIgnoreCase("bye")) {
                        System.out.println("Please enter a message with more than five words");
                        continue;
                    }

                    out.writeUTF(clientData);
                    System.out.println("Client sent data: " + clientData);
                    out.flush();

                    ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
                    if (clientData.equalsIgnoreCase("bye")) {
                        System.out.println("The client closes the connection to the server");
                        out.close();
                        in.close();
                        br.close();
                        clientSocket.close();
                        break;
                    }

                    SentenceMessage sentenceMessage = (SentenceMessage) in.readObject();
                    System.out.println("Received message from server: " +
                            sentenceMessage.getMessage());
                    System.out.println("Received processed data from the server: " +
                            sentenceMessage.getSentence());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}