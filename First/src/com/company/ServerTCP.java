package com.company;

import java.io.DataInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP extends Thread {
    ServerSocket serverSocket = null;

    public ServerTCP() {
        try {
            serverSocket = new ServerSocket(1500);
            System.out.println("Starting server on 1500 - port");

            start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        try {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Connection accepted from "
                        + clientSocket.getInetAddress().getHostAddress());

                while (!clientSocket.isClosed()) {
                    DataInputStream in = new DataInputStream(clientSocket.getInputStream());
                    String receivedData = in.readUTF();
                    System.out.println("Data received from the client: " + receivedData);

                    ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
                    String processedData = SentenceUtility.SortWords(receivedData);
                    SentenceMessage sentenceMessage =
                            new SentenceMessage(processedData, "Server processing message");
                    out.writeObject(sentenceMessage);

                    if (receivedData.equalsIgnoreCase("bye")) {
                        System.out.println("The client closes the connection to the server");
                        in.close();
                        out.close();
                        clientSocket.close();
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String args[]) {
        new ServerTCP();
    }
}