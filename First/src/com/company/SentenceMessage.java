package com.company;

import java.io.Serializable;
import java.util.List;

public class SentenceMessage implements Serializable {
    private String sentence;
    private String message;

    public SentenceMessage(String sentence, String message) {
        this.sentence = sentence;
        this.message = message;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
