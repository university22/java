<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 29.05.2021
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <div align="center">
    <h2>Car list</h2>
    <table border="1" cellpadding="5">
      <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Carrying</th>
        <th>Serviceable</th>
      </tr>
      <c:forEach items="${listCar}" var="car">
        <tr>
          <td>${car.id}</td>
          <td>${car.model}</td>
          <td>${car.carrying}</td>
          <td>${car.serviceable}</td>
        </tr>
      </c:forEach>
    </table>
  </div>
  </body>
</html>
