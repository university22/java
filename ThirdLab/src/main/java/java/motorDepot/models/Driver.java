package java.motorDepot.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Driver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String surname;
    //private Car car;
    private Integer carId;

    protected Driver(){

    }

    protected  Driver(String surname, Integer carId){
        this.carId = carId;
        this.surname = surname;
    }

    public Integer getCarId() {
        return carId;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getId() {
        return id;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
