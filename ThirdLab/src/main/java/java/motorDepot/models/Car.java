package java.motorDepot.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String model;
    private Integer carrying;
    private boolean serviceable;

    protected  Car(){

    }

    protected Car(String model, Integer carrying, boolean serviceable){
        this.carrying = carrying;
        this.model = model;
        this.serviceable = serviceable;
    }

    public Integer getCarrying() {
        return carrying;
    }

    public Integer getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public boolean isServiceable() {
        return serviceable;
    }

    public void setCarrying(Integer carrying) {
        this.carrying = carrying;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setServiceable(boolean serviceable) {
        this.serviceable = serviceable;
    }
}
