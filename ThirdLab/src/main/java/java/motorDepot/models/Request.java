package java.motorDepot.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer driverid ;
    private Integer cargoweight;
    private boolean completed;

    protected  Request(){

    }

    protected Request(Integer driveid, Integer cargoweight, boolean completed){
        this.driverid = driveid;
        this.cargoweight = cargoweight;
        this.completed = completed;
    }


}