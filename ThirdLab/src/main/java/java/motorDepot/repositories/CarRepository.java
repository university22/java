package java.motorDepot.repositories;

import java.motorDepot.models.Car;
import org.springframework.data.repository.CrudRepository;

public interface  CarRepository extends CrudRepository<Car, Integer>{
}
