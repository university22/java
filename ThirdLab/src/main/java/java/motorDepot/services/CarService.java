package java.motorDepot.services;

import java.motorDepot.models.Car;
import java.util.List;

import java.motorDepot.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarService {
    @Autowired CarRepository repo;

    public void save(Car car) {
        repo.save(car);
    }

    public List<Car> listAll() {
        return (List<Car>) repo.findAll();
    }

    public Car get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
