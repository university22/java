package java.motorDepot.controllers;

import java.motorDepot.services.CarService;
import java.motorDepot.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CarController {
    @Autowired private CarService carService;

    @RequestMapping("/")
    public ModelAndView home() {
        List<Car> listCar = carService.listAll();
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("listCar", listCar);
        return mav;
    }
}
